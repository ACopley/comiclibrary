from flask_wtf import FlaskForm
from wtforms import PasswordField, TextField, RadioField, IntegerField, SelectField
from wtforms.validators import DataRequired

class LoginF(FlaskForm):
    username = TextField('user', validators=[DataRequired()])
    password = PasswordField('pass', validators=[DataRequired()])

class SignUpF(FlaskForm):
    username = TextField('user', validators=[DataRequired()])
    password1 = PasswordField('pass1', validators=[DataRequired()])
    password2 = PasswordField('pass2', validators=[DataRequired()])

class AddF(FlaskForm):
    Type = RadioField('type', validators=[DataRequired()], choices=[('c1', 'Comic'), ('c2', 'Graphic Novel')])
    Universe = TextField('uni', validators=[DataRequired()])
    VolumeNumber = IntegerField('VolNo', validators=[DataRequired()])
    TypeNo = IntegerField('INo', validators=[DataRequired()])
    Character = TextField('char', validators=[DataRequired()])
    Author = TextField('author', validators=[DataRequired()])

class SearchF(FlaskForm):
    Thing = SelectField('thing', validators=[DataRequired()], choices=[('c1' ,'Character'), ('c2', 'Author')])
    Search = TextField('word', validators=[DataRequired()])

class NewPF(FlaskForm):
    prevp = PasswordField('passp', validators=[DataRequired()])
    passw1 = PasswordField('pass1', validators=[DataRequired()])
    passw2 = PasswordField('pass2', validators=[DataRequired()])
