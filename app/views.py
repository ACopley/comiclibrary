from app import app
from flask import render_template, redirect, url_for, session, request, flash
from .forms import LoginF, SignUpF, AddF, SearchF, NewPF
from app import db, models
import hashlib

@app.route('/')
def Home():
    if not session.get('SignedIn'):
        form = LoginF()
        message = False;
        if 'message' in session:
            message = session['message']
        session['message'] = False
        return render_template('Login.html', form=form, message=message)
    else:
        form = SearchF()
        All = models.Items.query.all()
        lenh = len(All)
        Char = []
        Auth = []
        for x in All:
            Char.append(models.Characters.query.get(x.Charid))
            Auth.append(models.Authors.query.get(x.Authorid))
        return render_template('Home.html', form=form, All=All, Char=Char, Auth=Auth, lenh=lenh)

@app.route('/Home', methods=['POST'])
def Login():
    username = request.form['username']
    password = request.form['password']
    session['username'] = username
    password = hashlib.md5(password.encode()).hexdigest()
    temp = models.Accounts.query.filter_by(uname=username, passw=password).all()
    if len(temp) is 1:
        session['SignedIn'] = True
    else:
        session['message'] = True
    return Home()

@app.route('/Logout')
def Logout():
    session['SignedIn'] = False
    return Home()

@app.route('/SignUp', methods=['POST'])
def SignUp():
    form = SignUpF()
    message = False;
    if 'message' in session:
        message = session['message']
    session['message'] = False
    return render_template('SignUp.html', form=form, message=message)

@app.route('/Sign_Up', methods=['POST'])
def SignUpGet():
    session['message'] = False
    username = request.form['username']
    password1 = request.form['password1']
    password2 = request.form['password2']
    temp = models.Accounts.query.filter_by(uname=username).all()
    if password1 == password2 and len(temp) == 0:
        password = hashlib.md5(password1.encode()).hexdigest()
        t = models.Accounts(uname=username, passw=password)
        db.session.add(t)
        db.session.commit()
        session['SignedIn'] = True
        return Home()
    session['message'] = True
    return SignUp()

@app.route('/AddItem')
def SetUpAdd():
    form = AddF()
    return render_template("AddItem.html", form=form)

@app.route('/ItemAdded', methods=['POST'])
def Add():
    AuthorS = request.form['Author']
    CharacterS = request.form['Character']
    if(request.form['Type'] == 'c1'):
        Yes = True
    else:
        Yes = False

    temp = models.Authors.query.filter_by(AuthorN=AuthorS).all()
    if(len(temp) == 1):
        AuthorKey = temp[0].id
    else:
        new = models.Authors(AuthorN=AuthorS)
        db.session.add(new)
        db.session.commit()
        temp = models.Authors.query.filter_by(AuthorN=AuthorS).all()
        AuthorKey = temp[0].id

    temp = models.Characters.query.filter_by(CharacterN=CharacterS).all()
    if(len(temp) == 1):
        CharacterKey = temp[0].id
    else:
        new = models.Characters(CharacterN=CharacterS)
        db.session.add(new)
        db.session.commit()
        temp = models.Characters.query.filter_by(CharacterN=CharacterS).all()
        CharacterKey = temp[0].id

    t = models.Items(Authorid=AuthorKey, Charid=CharacterKey,
    Universe=request.form['Universe'], VolNo=request.form['VolumeNumber'],
     Comic=Yes, TypeNo=request.form['TypeNo'])
    db.session.add(t)
    db.session.commit()
    return Home()

@app.route('/Search', methods=['POST'])
def Search():
    form = SearchF()
    item = request.form['Thing']
    what = request.form['Search']
    List = []
    if item == 'c1':
        temp = models.Characters.query.filter_by(CharacterN=what).first()
        key = temp.id
        items = models.Items.query.filter_by(Charid=key).all()
        for x in items:
             List.append(models.Authors.query.get(x.Authorid))
    else:
        temp = models.Authors.query.filter_by(AuthorN=what).first()
        key = temp.id
        items = models.Items.query.filter_by(Authorid=key).all()
        for x in items:
             List.append(models.Characters.query.get(x.Charid))
    return render_template("Search.html", items=items, item=item, what=what, List=List)

@app.route('/ChangeLoad', methods=['GET', 'POST'])
def ChangeLoad():
        form = NewPF()
        return render_template("Change.html", form=form)

@app.route('/ChangeP', methods=['GET', 'POST'])
def ChangeP():
        currentpassw = request.form['prevp']
        newpass1 = request.form['passw1']
        newpass2 = request.form['passw2']
        password = hashlib.md5(currentpassw.encode()).hexdigest()
        user = ""
        if 'username' in session:
            user = session['username']
        temp = models.Accounts.query.filter_by(uname=user, passw=password).all()
        if len(temp) == 1 and newpass1 == newpass2:
            for x in temp:
                x.passw = hashlib.md5(newpass2.encode()).hexdigest()
            db.session.commit()
        return Home()
