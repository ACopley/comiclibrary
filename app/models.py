from app import db

class Accounts(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    uname = db.Column(db.String(20), unique=True)
    passw = db.Column(db.String(25))

class Authors(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    AuthorN = db.Column(db.String(30))

class Characters(db.Model):
     id = db.Column(db.Integer, unique=True, primary_key=True)
     CharacterN = db.Column(db.String(30))

class Items(db.Model):
    Itemid = db.Column(db.Integer, unique=True, primary_key=True)
    Authorid = db.Column(db.Integer, db.ForeignKey(Authors.id))
    Charid = db.Column(db.Integer, db.ForeignKey(Characters.id))
    Universe = db.Column(db.String(20))
    VolNo = db.Column(db.Integer)
    Comic = db.Column(db.Boolean)
    TypeNo = db.Column(db.Integer)
